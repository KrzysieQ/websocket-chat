const express = require('express');
const path = require('path');
const socket = require('socket.io');
const app = express();
const PORT = process.env.PORT || 8000;
const messages = [];
const users = [];

const server = app.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`);
});
const io = socket(server);

io.on('connection', (socket) => {
  socket.on('message', (message) => {
    messages.push(message);
    socket.broadcast.emit('message', message);
  });
  socket.on('join', (username) => {
    users.push({ name: username, id: socket.id });
    socket.broadcast.emit('message', {author: 'Chat Bot', content: `<span class="system">User-${username} has joined the conversation!</span>`})

  });
  socket.on('disconnect', () => {
    const user = users.findIndex(idx => idx.id === socket.id);
    if(user >= 0) {
      socket.broadcast.emit('message', {author: 'Chat Bot', content: `<span class="system">User-${users[user].name} has left the conversation!</span>`})
      users.splice(user, 1);
    }
  });
});

app.use(express.static(path.join(__dirname, '/client')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/client/index.html'));
});

